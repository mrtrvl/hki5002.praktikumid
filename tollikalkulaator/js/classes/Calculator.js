class Calculator {

    constructor(layerName) {
        this.layer = document.getElementById(layerName);
        this.layer.innerHTML = 'Tollid: <input type="number" id="inches" />' +  // Html
            '<input type="button" value="Sentimeetriteks" id="calculateButton"/>' +
            '<div id="answer"></div>';
        this.inches = document.getElementById("inches");      // Veebilehe tollide väli
        this.answerLayer = document.getElementById("answer"); // Veebilehe vastuse väli
    }

    calculate() { // Arvutamise meetod
        let isInputValid = Calculator.validateInput(this.inches.value); // Kontrollib, kas sisestatud väärtus on korrektne
        if (isInputValid) { // Kui sisestatud väärtus oli number
            const CENTIMETERS_IN_INCH = 2.54;
            this.answerLayer.innerHTML = parseFloat(this.inches.value) * CENTIMETERS_IN_INCH + " cm"; // Arvutab vastuse ja väljastab vastuse
        } else {    // Kui sisestatud väärtus ei olnud korrektne
            alert("Sisestus ei ole korrektne!"); // Väljastab veateate
        }

    }

    static validateInput(input) { // Kontrollib, kas muutuja on number ja ei ole tühi
        return !(isNaN(input) || input === '');
    }
}