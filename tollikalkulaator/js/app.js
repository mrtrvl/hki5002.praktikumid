window.onload = function () {

    let calculator = new Calculator('layer1'); // Uus kalkulaatori objekt

    document.getElementById("calculateButton").onclick = function () { // Kui vajutatakse htmlis nuppu
        calculator.calculate();         // Kutsub calculate meetodi kalkulaatori objektist
    }
};

