class ReadFromServer {
    constructor(layerName, filename, buttonText) {

        this.layerName = layerName;
        this.filename = filename;
        this.buttonText = buttonText;

        this.xhr = new XMLHttpRequest();

        this.element = document.createElement('div');
        this.element.setAttribute('id', this.layerName);

        document.querySelector('#layer').appendChild(this.element);

        this.switchButton = this.element.appendChild(HtmlElement.button(this.buttonText));
        this.switchButton.addEventListener('click', this.readFromServer.bind(this));

        this.answer = document.createElement('div');
        this.answer = this.element.appendChild(this.answer);
        this.answer.setAttribute('id', this.layerName + 'answer');
    }

    readFromServer() {

        this.xhr.open("GET", this.filename, false);
        this.xhr.send();

        this.xhr.onreadystatechange = this.dataIn();
    }

    dataIn() {

        if (this.xhr.readyState == 4) {

            document.getElementById(this.layerName + 'answer').innerHTML = this.xhr.responseText;
        }
    }
}