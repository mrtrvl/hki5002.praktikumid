# README #

See projekt on mõeldud Programmerimine 1 koduse projekti jaoks.
Projektiks on javascriptis tehtud html canvasel tile based võrgus mängitav tankimäng.

Peaks olema siis midagi sellist, kus väljakul sõidad tankiga ringi ja hävitad vaenlase sihtmärke, milleks võivad olla mingid suurtükitornid, tankid vms...

Kasutatud on p5.js frameworki - https://p5js.org/
Node.js ja socket.io õpetus - https://www.youtube.com/watch?v=HZWmrt3Jy10
ES6 süntaks -   http://ccoenraets.github.io/es6-tutorial/
                http://es6-features.org/#Constants
                https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
            
Mängu taustapilt: https://media.defense.gov/2016/Aug/24/2001614387/-1/-1/0/160719-M-HX324-700.JPG