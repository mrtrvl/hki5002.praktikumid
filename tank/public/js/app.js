setup = () => { // Käivitatakse ainult ühe korra
    gameplay = new Gameplay();

    gameplay.socket = io.connect('localhost:3333');

    createCanvas(gameplay.canvasSizeXAxis, gameplay.canvasSizeYAxis);

    let tankStartPositionX = gameplay.tankSize + random(width - gameplay.tankSize * 2);
    let tankStartPositionY = gameplay.tankSize + random(height - gameplay.tankSize * 2);

    gameplay.tank = new Tank(tankStartPositionX, tankStartPositionY);
    gameplay.tank.debug = true;

    gameplay.socket.emit('start', gameplay.tank); // Saadab serverile info uue tanki kohta

    gameplay.socket.on('heartbeat', function (infoFromServer) { // Serverilt tulev info
        gameplay.tanks = infoFromServer.tanks;
        gameplay.allBullets = infoFromServer.bullets;
        gameplay.wall = infoFromServer.wall;
    });
}

draw = () => { // Käivitatakse iga framega
    background(51);
    manageTank();
    manageBullets();
    manageWallPieces();
    document.getElementById('shots').innerHTML = 'Laske tehtud: ' + gameplay.shots;
}

manageWallPieces = () => {
    gameplay.wall.forEach(pieceOfWall => gameplay.show(pieceOfWall, 'wall'));
}

manageTank = () => { // Tankide asukoha uuendamine, joonistamine

    let lastX = gameplay.tank.x;
    let lastY = gameplay.tank.y;
    let lastAngle = gameplay.tank.angle;

    gameplay.tank.update(); // Tanki asukoha uuendamine

    let tankData = {
        id: gameplay.socket.id, // Andmed serverile saatmiseks
        tank: gameplay.tank
    };

    gameplay.socket.emit('update', tankData); // Saadab serverile andmed tanki asukoha kohta

    gameplay.tank.show(); // Joonistab oma tangi
    showOtherTanks(); // Joonistab teised tangid  
}

initBullet = () => { // Uue kuuli massiivi lisamine
    gameplay.shots++;
    gameplay.playerBullets.push(new Bullet(gameplay.tank.x,
        gameplay.tank.y,
        gameplay.tank.angle));
}

manageBullets = () => { // Kuulide uue asukoha arvutamine, kas kuul on väljakul, kuuli näitamine
    if (gameplay.playerBullets.length > 0) { // Kas kuulide massiivis üldse on kuule?
        for (let i = 0; i < gameplay.playerBullets.length; i++) {
            let bullet = gameplay.playerBullets[i];
            bullet.update();
            checkContactBetweenBulletAndWall(bullet) && (bullet.onField = false); // Kui kuul läheb vastu seinaelementi, siis määratakse kuul väljakult väljas olevaks
            if (bullet.outOfField() || !bullet.onField) { // Kui kuul on väljakult väljas, siis eemaldame massiivist
                gameplay.playerBullets.splice(i, 1);
            } else {
                bullet.show(); // Joonista kuul
            }
        }
        let bulletDataToServer = {
            id: gameplay.socket.id,
            bullets: gameplay.playerBullets
        };
        gameplay.socket.emit('bullets', bulletDataToServer); // Kuulide info saatmine serverile
    }

    showOtherBullets();
}

showOtherTanks = () => { // Joonistab väja teised tankid vastavalt serverilt saadud andmetele
    for (let i = 0; i < gameplay.tanks.length; i++) {
        if (gameplay.socket.id !== gameplay.tanks[i].id) {
            gameplay.show(gameplay.tanks[i].tank, 'tank')
        }
    }
}

showOtherBullets = () => { // Joonistab kuulid, mis ei ole mängija enda omad
    for (let i = 0; i < gameplay.allBullets.length; i++) {
        if (gameplay.socket.id !== gameplay.allBullets[i].id) {
            for (let j = 0; j < gameplay.allBullets[i].bullets.length; j++) {
                let bullet = gameplay.allBullets[i].bullets[j];
                gameplay.show(bullet, 'bullet');
                gameplay.checkIfTwoObjectOverlapping(gameplay.tank, bullet) && youLost(); // Kontrollib, kas tank on kuuliga pihta saanud
            }
        }
    }
}

checkContactBetweenBulletAndWall = (bullet) => {
    for (let i = 0; i < gameplay.wall.length; i++) {
        if (gameplay.checkIfTwoObjectOverlapping(bullet, gameplay.wall[i])) {
            return true;
        }
    }
    return false;
}

youLost = () => {
    document.getElementById('info').style.color = 'red';
    document.getElementById('info').innerHTML = 'Kaotasid!';
    gameplay.socket.emit('lost', gameplay.socket.id);
}

keyPressed = () => {
    const forward = 1;
    const backward = -1;
    const left = -1;
    const right = 1;

    keyCode === RIGHT_ARROW && gameplay.tank.rotateTank(left);
    keyCode === LEFT_ARROW && gameplay.tank.rotateTank(right);
    keyCode === UP_ARROW && gameplay.tank.action(forward);
    keyCode === DOWN_ARROW && gameplay.tank.action(backward);
    keyCode === gameplay.shootKeyCode && initBullet();
}