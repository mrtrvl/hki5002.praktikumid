class Gameplay {
    constructor() {
        this.socket;
        this.id;
        this.tank;
        this.tankSize = 40;
        this.tanks = [];
        this.fieldRows = 12;
        this.fieldColumns = 15;
        this.wallPieceSize = 40;
        this.canvasSizeXAxis = this.fieldColumns * this.wallPieceSize;
        this.canvasSizeYAxis = this.fieldRows * this.wallPieceSize;
        this.shootKeyCode = 32;
        this.playerBullets = [];
        this.allBullets = [];
        this.wall = [];
        this.shots = 0;
    }

    show(object, objectType) { // Elemendi joonistamine
        rectMode(CENTER);
        noStroke();
        if (objectType === 'tank') {
            fill(0, 0, 255);
        } else if (objectType === 'bullet') {
            fill(255, 0, 0);
        } else if (objectType === 'wall') {
            fill(0, 255, 0);
        }

        push();
        translate(object.x, object.y);
        rotate(radians(object.angle));
        rect(0, 0, object.size, object.size);
        pop();
    }

    checkIfTwoObjectOverlapping(firstObject, secondObject) {

        let firstBoxWidth = firstObject.size;
        let firstBoxLeft = firstObject.x;
        let firstBoxRight = firstBoxLeft + firstBoxWidth;
        let firstBoxTop = firstObject.y;
        let firstBoxBottom = firstBoxTop + firstBoxWidth;

        let secondBoxWidth = secondObject.size;
        let secondBoxLeft = secondObject.x;
        let secondBoxRight = secondBoxLeft + secondBoxWidth;
        let secondBoxTop = secondObject.y;
        let secondBoxBottom = secondBoxTop + secondBoxWidth;

        return ((Math.abs(firstBoxLeft - secondBoxLeft) < firstBoxWidth) &&
            (firstBoxLeft < secondBoxRight) &&
            (Math.abs(firstBoxTop - secondBoxTop) < firstBoxWidth) &&
            firstBoxTop < secondBoxBottom);
    }
}