var tanks = [];
var bullets = [];
var wall = [];

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use(express.static('public'));

wall = [{
        x: 100,
        y: 100,
        angle: 0,
        size: 40
    },
    {
        x: 150,
        y: 150,
        angle: 0,
        size: 40
    },
    {
        x: 200,
        y: 200,
        angle: 0,
        size: 40
    },
    {
        x: 250,
        y: 250,
        angle: 0,
        size: 40
    }
];

io.on('connection', function (socket) {
    console.log('new connection: ' + socket.id);

    socket.on('start', function (tankDataWithoutId) { // Kui klient saadab uue tanki andmed
        tanks.push({
            id: socket.id,
            tank: tankDataWithoutId
        });
    });

    socket.on('update', function (tankDataWithId) { // Kui klient uuendab tanki andmeid
        for (var i = 0; i < tanks.length; i++) {
            if (socket.id === tanks[i].id) {
                tanks[i].tank = tankDataWithId.tank;
            }
        }
    });

    socket.on('bullets', function (bulletsFromClient) { // Kuulide info klientidelt      
        if (bullets.length !== 0) {
            for (var i = 0; i < bullets.length; i++) {
                if (bullets[i].id === bulletsFromClient.id) {
                    bullets[i].bullets = bulletsFromClient.bullets;
                }
                if (bullets[i].bullets.length < 1) {
                    bullets.splice(i, 1);
                }
            }
        } else {
            if (bulletsFromClient.bullets.length > 0) {
                bullets.push(bulletsFromClient);
            }
        }
    });

    socket.on('disconnect', function () { // Kui klient katkestab ühenduse
        console.log('disconnected: ' + socket.id);
        for (var i = 0; i < tanks.length; i++) {
            if (socket.id === tanks[i].id) {
                tanks.splice(i, 1);
            }
        }
    });

    socket.on('lost', function () { // Kui klient teatab, et ta on kaotanud
        for (var i = 0; i < tanks.length; i++) {
            if (socket.id === tanks[i].id) {
                tanks.splice(i, 1);
            }
        }
    });
});

setInterval(heartbeat, 33);

function heartbeat() { // Info saatmine klientidele
    var dataForClients = {
        tanks: tanks,
        bullets: bullets,
        wall: wall
    }
    io.sockets.emit('heartbeat', dataForClients);
}


http.listen(3333, function () {
    console.log('listening on *:3333');
});