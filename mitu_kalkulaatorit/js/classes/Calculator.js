class Calculator{
    constructor(kihinimi){
        this.kiht=document.getElementById(kihinimi);
        window[kihinimi+"_kalkulaator"]=this;
        this.kiht.innerHTML=
            "Tollid: <input type='text' id='"+kihinimi+"_kast1' /> "+
            "<input type='button' value='Sentimeetriteks' "+
            "onClick='"+kihinimi+"_kalkulaator.arvuta();' /> "+
            "<div id='"+kihinimi+"_vastus'></div>";
        this.kast=document.getElementById(kihinimi+"_kast1");
        this.vastusekiht=document.getElementById(kihinimi+"_vastus");
    }
    arvuta(){
        const CMS_IN_INCHES = 2.54;
        this.vastusekiht.innerHTML=parseFloat(this.kast.value)*CMS_IN_INCHES;
    }
}
