class Calculator {
    constructor(layerName, boxText, buttonText, multiplier) {

        this.element = document.createElement('div');
        this.element.setAttribute('id', layerName + '_calculator');

        this.boxText = document.createTextNode(boxText);

        this.box = document.createElement('input');
        this.box.type = 'text';

        this.button = document.createElement('input');
        this.button.type = 'button';
        this.button.value = buttonText;
        this.button.addEventListener('click', this.calculate.bind(this));

        this.answerField = document.createElement('span');
        this.answerField.setAttribute('id', layerName + '_answer');

        document.querySelector('#Calculators').appendChild(this.element);

        this.element.appendChild(this.boxText);
        this.element.appendChild(this.box);
        this.element.appendChild(this.button);
        this.element.appendChild(this.answerField);

        this.multiplier = multiplier;
    }

    calculate() {
        this.input = parseFloat(this.box.value);
        this.valid = this.validateInput(this.input);
        if (this.valid){
            this.answer = this.input * this.multiplier;
            this.answerField.innerHTML = this.answer;
        } else {
            alert("Sisestus ei ole korrektne!");
        }
    }

    validateInput(input) { //
        return !(isNaN(input) || input === '');
    }
}