class Gameplay {
    constructor(){
        this.socket;
        this.id;
        this.tank;
        this.tankSize = 40;
        this.tanks = [];
        this.fieldRows = 10;
        this.fieldColumns = 10;
        this.wallPieceSize = 40;
        this.canvasSizeXAxis = this.fieldColumns * this.wallPieceSize;
        this.canvasSizeYAxis = this.fieldRows * this.wallPieceSize;
        this.shootKeyCode = 32;
        this.playerBullets = [];
        this.allBullets = [];
    }

    show(xLocation, yLocation, angle, size, objectType) { // Elemendi joonistamine
        rectMode(CENTER);
        noStroke();
        if (objectType === 'tank'){
            fill(0, 0, 255);
        } else {
            fill(255, 0, 0);
        }
        
        push();
        translate(xLocation, yLocation);
        rotate(radians(angle));
        rect(0, 0, size, size);
        pop();
    }

    checkIfTwoObjectOverlapping(firstObject, secondObject){

        let firstBoxWidth = firstObject.width;
        let firstBoxLeft = firstObject.x;
        let firstBoxRight = firstBoxLeft + firstBoxWidth;
        let firstBoxTop = firstObject.y;
        let firstBoxBottom = firstBoxTop + firstBoxWidth;

        let secondBoxWidth = secondObject.width;
        let secondBoxLeft = secondObject.x;
        let secondBoxRight = secondBoxLeft + secondBoxWidth;
        let secondBoxTop = secondObject.y;
        let secondBoxBottom = secondBoxTop + secondBoxWidth;

        if ((Math.abs(firstBoxLeft - secondBoxLeft) < firstBoxWidth) &&
            (firstBoxLeft < secondBoxRight) &&
            (Math.abs(firstBoxTop - secondBoxTop) < firstBoxWidth) &&
            firstBoxTop < secondBoxBottom) {
                
                return true;
        }
        return false;
    }
}
