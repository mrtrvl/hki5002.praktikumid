function setup() { // Käivitatakse ainult ühe korra
    gameplay = new Gameplay();

    gameplay.socket = io.connect('localhost:3333');

    createCanvas(gameplay.canvasSizeXAxis, gameplay.canvasSizeYAxis);

    let tankStartPositionX = gameplay.tankSize + random(width - gameplay.tankSize * 2);
    let tankStartPositionY = gameplay.tankSize + random(height - gameplay.tankSize * 2);

    gameplay.tank = new Tank(tankStartPositionX, tankStartPositionY);
    gameplay.tank.debug = true;

    let tankDataToServer = {
        xLocation: gameplay.tank.x,
        yLocation: gameplay.tank.y,
        angle: gameplay.tank.angle,
        size: gameplay.tank.size
    };

    gameplay.socket.emit('start', tankDataToServer); // Saadab serverile info uue tanki kohta

    gameplay.socket.on('heartbeat', function (infoFromServer) { // Serverilt tulev info
        gameplay.tanks = infoFromServer.tanks;
        gameplay.allBullets = infoFromServer.bullets;
    });
}

function draw() {       // Käivitatakse iga framega
    background(51);
    manageTank();
    manageBullets();
}

function manageTank() { // Tankide asukoha uuendamine, joonistamine
    gameplay.tank.update(); // Tangi asukoha uuendamine

    let tankData = {
        id: gameplay.socket.id, // Andmed serverile saatmiseks
        tank: {
            xLocation: gameplay.tank.x,
            yLocation: gameplay.tank.y,
            angle: gameplay.tank.angle,
            size: gameplay.tank.size
        }
    };

    gameplay.socket.emit('update', tankData); // Saadab serverile andmed tanki asukoha kohta

    gameplay.tank.show();   // Joonistab oma tangi
    showOtherTanks();       // Joonistab teised tangid  
}

function initBullet() {     // Uue kuuli massiivi lisamine
    gameplay.playerBullets.push(new Bullet(gameplay.tank.x,
        gameplay.tank.y,
        gameplay.tank.angle));
}

function manageBullets() {  // Kuulide uue asukoha arvutamine, kas kuul on väljakul, kuuli näitamine
    if (gameplay.playerBullets.length > 0) { // Kas kuulide massiivis üldse on kuule?
        for (let i = 0; i < gameplay.playerBullets.length; i++) {
            gameplay.playerBullets[i].update();
            if (gameplay.playerBullets[i].outOfField()) { // Kui kuul on väljakult väljas, siis eemaldame massiivist
                gameplay.playerBullets.splice(i, 1);
            } else {
                gameplay.playerBullets[i].show(); // Joonista kuul
            }
        }
        let bulletDataToServer = {
            id: gameplay.socket.id,
            bullets: gameplay.playerBullets
        };
        gameplay.socket.emit('bullets', bulletDataToServer); // Kuulide info saatmine serverile
    }

    showOtherBullets();
}

function showOtherTanks() { // Joonistab väja teised tankid vastavalt serverilt saadud andmetele
    for (let i = 0; i < gameplay.tanks.length; i++) {
        if (gameplay.socket.id !== gameplay.tanks[i].id) {

            gameplay.show(gameplay.tanks[i].tank.xLocation,
                gameplay.tanks[i].tank.yLocation,
                gameplay.tanks[i].tank.angle,
                gameplay.tanks[i].tank.size,
                'tank')

            let tank1 = {
                x: gameplay.tank.x,
                y: gameplay.tank.y,
                width: gameplay.tank.size
            };

            let tank2 = {
                x: gameplay.tanks[i].tank.xLocation,
                y: gameplay.tanks[i].tank.yLocation,
                width: gameplay.tanks[i].tank.size
            }

            if (gameplay.checkIfTwoObjectOverlapping(tank1, tank2)) {
                console.log('kattuvad');
            }
        }
    }
}

function showOtherBullets() { // Joonistab kuulid, mis ei ole mängija enda omad
    for (let i = 0; i < gameplay.allBullets.length; i++) {
        if (gameplay.socket.id !== gameplay.allBullets[i].id) {
            for (let j = 0; j < gameplay.allBullets[i].bullets.length; j++) {
                gameplay.show(gameplay.allBullets[i].bullets[j].x,
                              gameplay.allBullets[i].bullets[j].y,
                              gameplay.allBullets[i].bullets[j].angle,
                              gameplay.allBullets[i].bullets[j].size,
                              'bullet');

                let tank = {
                    x: gameplay.tank.x,
                    y: gameplay.tank.y,
                    width: gameplay.tank.size
                };

                let bullet = {
                    x: gameplay.allBullets[i].bullets[j].x,
                    y: gameplay.allBullets[i].bullets[j].y,
                    width: gameplay.allBullets[i].bullets[j].size
                }

                if (gameplay.checkIfTwoObjectOverlapping(tank, bullet)) { // Kontrollib, kas tank on kuuliga pihta saanud
                    youLost();
                }
            }
        }
    }
}

function youLost() {
    console.log('Kaotasid!');
    gameplay.socket.emit('lost', gameplay.socket.id);
}

function keyPressed() {
    if (keyCode === RIGHT_ARROW) {
        gameplay.tank.rotateTank(-1);
    }

    if (keyCode === LEFT_ARROW) {
        gameplay.tank.rotateTank(1);
    }

    if (keyCode === UP_ARROW) {
        gameplay.tank.action(1);
    }

    if (keyCode === DOWN_ARROW) {
        gameplay.tank.action(-1);
    }

    if (keyCode === gameplay.shootKeyCode) {
        initBullet();
    }
}