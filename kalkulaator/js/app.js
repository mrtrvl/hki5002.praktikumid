function arvuta(){ // Arvutab saadava valuutakoguse etteantud rahasumma, kursi ja komisjonitasu järgi
    let raha = parseFloat(document.getElementById('raha').value);
    let kurss = parseFloat(document.getElementById("kurss").value);
    let komisjonitasu = parseFloat(document.getElementById("komisjonitasu").value);

    if (isNaN(raha) || isNaN(kurss) || isNaN(komisjonitasu)){ // Kontrollib, kas kõik väljad sisaldavad õiget infot
        alert("Palun täida kõik väljad");
    } else if(raha <= 0 || kurss <= 0){
        alert("Raha summa ja / või kurss ei tohi olla 0 või negatiivne number!");
    } else if(komisjonitasu < 0){
        alert("Komisjonitasu ei saa olla negatiivne!");
    } else {    // Kui kõik on korras, siis arvutab ja väljastab vastuse
        raha = raha - komisjonitasu;
        let vastus = raha * kurss;
        vastus = vastus.toFixed(2);
        document.getElementById("vastus").innerHTML = vastus;
    }
}