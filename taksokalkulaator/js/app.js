window.onload = function() {
    const START_FEE = 3.85;         // Sõidu alustamise tasu
    const PRICE_PER_KM_DAY = 0.69;  // Km hind päeval
    const PRICE_PER_KM_NIGHT = 0.8; // Km hind öösel
    let pricePerKm;

    getDayNightSelection();         // Päev / öö

    document.getElementById("startFee").innerHTML = START_FEE;  // Sõidu alustamise tasu väljastamine lehele

    document.getElementById("dayNight").onchange = function (){ // Kui päeva / öö valikut muudetakse...
        getDayNightSelection();
    };


    document.getElementById("calculateButton").onclick = function () {  // Sõidu hinna aravutamise ja vastuse väljastamise funktsioon
        let tripLength = parseFloat(document.getElementById('tripLength').value);

        if (!tripLength <= 0){  // Kui sisestatud teepikkus on suurem nullist
            answer = tripLength * pricePerKm + START_FEE;
            document.getElementById("answer").innerHTML = answer.toFixed(2) + " EUR";
        } else {                // Kui sisestatud teepikkus on nullist väiksem
            alert ("Teekonnapikkus peab olema suurem nullist!");
        }
    };

    function getDayNightSelection(){ // Funktsioon loeb päeva / öö valiku ja väljastab lehele vastava km hinna
        let dayNight = document.getElementById("dayNight").value;
        let pictureSource;

        if (dayNight === 'day'){
            pricePerKm = PRICE_PER_KM_DAY;
            pictureSource = 'img/day.jpg';
        } else {
            pricePerKm = PRICE_PER_KM_NIGHT;
            pictureSource = 'img/night.jpg';
        }
        document.getElementById('dayNightPicture').src=pictureSource;
        document.getElementById("kmPrice").innerHTML = pricePerKm;
    }
};

