class Calculator {

    constructor(layerName){
        this.layer = document.getElementById(layerName);
        this.layer.innerHTML = 'Ringi raadius sentimeetrites: <input type="number" id="radius" />' +  // Html
            '<input type="button" value="Pindala" id="calculateButton"/>' +
            '<div id="answer"></div>';
        this.radius=document.getElementById("radius");      // Veebilehe raadiuse väli
        this.answerLayer=document.getElementById("answer"); // Veebilehe vastuse väli
    }

    calculate (){ // Arvutamise meetod
        let radius = parseFloat(this.radius.value);     // Väärtus raadiuse väljalt
        let isInputValid = this.validateInput(radius);  // Kontrollib, kas sisestatud väärtus on korrektne
        if (isInputValid){                              // Kui sisestatud väärtus oli number
            let answer = Math.PI * Math.pow(radius, 2); // Arvutab ringi pindala
            this.answerLayer.innerHTML = answer + ' cm2';        // Väljastab vastuse
        } else {                                        // Kui sisestatud väärtus ei olnud korrektne
            alert("Sisestus ei ole korrektne!");        // Väljastab veateate
        }

    }

    validateInput(input){ // Kontrollib, kas muutuja on positiivne number ja ei ole tühi
        if (isNaN(input) || input === '' || input < 0){
            return false;
        } else {
            return true;
        }
    }
}