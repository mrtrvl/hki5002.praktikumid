class ReadFromServer {
    constructor(layerName, buttonText) {

        this.layerName = layerName;
        this.buttonText = buttonText;

        this.xhr = new XMLHttpRequest();

        this.createHtml();
    }

    readFromServer() {

        this.filename = this.dropDown.value;

        this.xhr.open("POST", this.filename, false);
        this.xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

        this.xhr.send();

        this.xhr.onreadystatechange = this.dataIn();
    }

    dataIn() {

        if (this.xhr.readyState == 4) {

            document.getElementById(this.layerName + 'answer').innerHTML = this.xhr.responseText;
        }
    }

    createHtml(){
        this.element = document.createElement('div');
        this.element.setAttribute('id', this.layerName);

        this.jsonForDropDown = {'Uudis 1': 'news1.txt', 'Uudis 2': 'news2.txt', 'Uudis 3': 'news3.txt'};

        document.querySelector('#layer').appendChild(this.element);

        this.dropDown = this.element.appendChild(HtmlElement.dropDown(this.jsonForDropDown));

        this.switchButton = this.element.appendChild(HtmlElement.button(this.buttonText));
        this.switchButton.addEventListener('click', this.readFromServer.bind(this));

        this.answer = document.createElement('div');
        this.answer = this.element.appendChild(this.answer);
        this.answer.setAttribute('id', this.layerName + 'answer');
    }
}