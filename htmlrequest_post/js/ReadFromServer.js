class ReadFromServer {
    constructor(layerName, filename, buttonText) {

        this.layerName = layerName;
        this.filename = filename;
        this.buttonText = buttonText;

        this.xhr = new XMLHttpRequest();

        this.createHtml();

        this.xhr.onreadystatechange = function(){
            this.dataIn();
        }.bind(this);
    }

    readFromServer() {

        this.xhr.open("POST", this.filename, true);
        this.xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

        this.xhr.send(
            "firstname="+encodeURI(this.firstName.value)+
            "&lastname="+encodeURI(this.lastName.value));
    }

    dataIn() {

        if (this.xhr.readyState == 4) {

            document.getElementById(this.layerName + 'answer').innerHTML = this.xhr.responseText;
        }
    }

    createHtml(){
        this.element = document.createElement('div');
        this.element.setAttribute('id', this.layerName);

        document.querySelector('#layer').appendChild(this.element);

        this.firstName = HtmlElement.textField('Eesnimi');
        this.firstName = this.element.appendChild(this.firstName);

        this.lastName = HtmlElement.textField('Perekonnanimi');
        this.lastName = this.element.appendChild(this.lastName);

        this.switchButton = this.element.appendChild(HtmlElement.button(this.buttonText));
        this.switchButton.addEventListener('click', this.readFromServer.bind(this));

        this.answer = document.createElement('div');
        this.answer = this.element.appendChild(this.answer);
        this.answer.setAttribute('id', this.layerName + 'answer');
    }
}