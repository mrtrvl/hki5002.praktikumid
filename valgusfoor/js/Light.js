class Light {
    constructor(colour, state, layerName) {
        this.colour = colour;
        this.state = this.validateState(state);
        this.canvasWidth = 50;
        this.canvasHeight = 50;


        this.element = document.createElement('div');
        this.element.setAttribute('id', layerName);

        document.querySelector('#Light').appendChild(this.element);

        this.canvas = new Canvas(this.canvasWidth, this.canvasHeight, this.element);

        this.switchButton = this.element.appendChild(HtmlElement.button('Muuda olekut'));
        this.switchButton.addEventListener('click', this.stateChange.bind(this));

        this.drawLight(this.colour, this.state);
    }

    stateQuery() {
        return this.state;
    }

    stateChange() {
        this.state = (this.state !== true);
        this.drawLight(this.colour, this.state);
        console.log(this.stateQueryAsText());
    }

    stateQueryAsText() {
        return (this.state === true) ? this.colour + ': on' : this.colour + ': off';
    }

    drawLight(colour, fill) {
        this.x = 25;
        this.y = 25;
        this.radius = 20;
        this.colour = colour;
        this.fill = fill;

        this.canvas.circle(this.x, this.y, this.radius, this.colour, this.state);
    }

    validateState(state) {

        if (typeof state === 'boolean') {
            return state;
        }

        return false;
    }
}