function joonista(){
    var g = document.getElementById("tahvel").getContext("2d");
    var laius = parseInt(document.getElementById("laiuskast").value);
    var korgus = parseInt(document.getElementById("korguskast").value);
    var vasakult = parseInt(document.getElementById("vasakkast").value);
    var ylalt = parseInt(document.getElementById("ylakast").value);
    g.fillStyle = "yellow";
    g.fillRect(0, 0, 300, 200);
    g.fillStyle = "black";
    g.fillRect(vasakult, ylalt, laius, korgus); //x, y, laius, kõrgus
    g.fillText(korgus, vasakult-20, ylalt+20);
    g.fillText(laius, vasakult+20, ylalt-20);
}