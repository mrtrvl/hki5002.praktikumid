class ReadFromServer {
    constructor(layerName, filename) {

        this.layerName = layerName;
        this.filename = filename;

        this.xhr = new XMLHttpRequest();

        this.createHtml();

        this.xhr.onreadystatechange = function(){
            this.dataIn();
        }.bind(this);
    }

    readFromServer() {

        this.xhr.open("POST", this.filename, true);
        this.xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

        this.xhr.send();
    }

    dataIn() {

        if (this.xhr.readyState == 4) {

            document.getElementById(this.layerName + 'answer').innerHTML = this.xhr.responseText;
        }
    }

    createHtml(){
        this.element = document.createElement('div');
        this.element.setAttribute('id', this.layerName);

        document.querySelector('#layer').appendChild(this.element);

        this.answer = document.createElement('div');
        this.answer = this.element.appendChild(this.answer);
        this.answer.setAttribute('id', this.layerName + 'answer');
    }
}