class Canvas {
    constructor(sizeX, sizeY, object) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.canvas = object.appendChild(HtmlElement.canvas(this.sizeX, this.sizeY));
    }

    rectangle(x, y, width, height, colour) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.colour = colour;

        this.drawingArea=this.canvas;
        this.context = this.drawingArea.getContext("2d");
        this.context.fillStyle=this.colour;
        this.context.clearRect(0, 0, this.sizeX, this.sizeY);
        this.context.fillRect(this.x, this.y, this.width, this.height);
    }
}