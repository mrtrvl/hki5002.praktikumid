class HtmlElement {

    static numberField(initialValue) {
        const numberField = document.createElement('input');
        numberField.type = 'number';
        numberField.value = initialValue;

        return numberField;
    }

    static button(text) {
        const button = document.createElement('input');
        button.type = 'button';
        button.value = text;

        return button;
    }

    static lineBreak() {
        const lineBreak = document.createElement('br');

        return lineBreak;
    }

    static div(idName) {
        const div = document.createElement('div');
        div.setAttribute('id', idName);

        return div;
    }

    static slider(min, max, step) {
        const slider = document.createElement('input');
        slider.type = 'range';
        slider.min = min;
        slider.max = max;
        slider.step = step;

        return slider;
    }

    static canvas(sizeX, sizeY) {
        const canvas = document.createElement('canvas');
        canvas.width = sizeX;
        canvas.height = sizeY;

        return canvas;
    }

    static dropDown(inputInJson) {
        const dropDown = document.createElement('select');
        for (let name in inputInJson) {
            const option = document.createElement('option');
            option.value = inputInJson[name];
            option.textContent = name;
            dropDown.appendChild(option);
        }

        return dropDown;
    }
}
