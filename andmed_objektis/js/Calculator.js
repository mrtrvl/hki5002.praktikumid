class Calculator {
    constructor(layerName, increaseText, decreaseText, sum) {
        this.sum = sum;
        this.jsonForDropDown = {'1': 1, '2': 2, '5': 5, '10': 10};

        this.element = document.createElement('div');
        this.element.setAttribute('id', layerName);

        document.querySelector('#Calculator').appendChild(this.element);

        this.answerField = this.element.appendChild(HtmlElement.div('answerField'));

        this.decreaseButton = this.element.appendChild(HtmlElement.button(decreaseText));
        this.decreaseButton.addEventListener('click', this.decrease.bind(this));

        this.dropDown = this.element.appendChild(HtmlElement.dropDown(this.jsonForDropDown));

        this.increaseButton = this.element.appendChild(HtmlElement.button(increaseText));
        this.increaseButton.addEventListener('click', this.increase.bind(this));

        this.setToZeroButton = this.element.appendChild(HtmlElement.button('Nulli laoseis'));
        this.setToZeroButton.addEventListener('click', this.setToZero.bind(this));

        this.showAnswer();
    }

    increase() {
        this.increaseValue = this.getChangeValue();
        this.sum += this.increaseValue;
        this.showAnswer();
    }

    decrease() {
        this.decreaseValue = this.getChangeValue();
        this.sum -= this.decreaseValue;
        this.showAnswer();
    }

    showAnswer() {
        this.answerField.innerHTML = 'Lao kogus: ' + this.sum;
    }

    getChangeValue() {
        this.input = parseFloat(this.dropDown.value);
        this.valid = Calculator.validateInput(this.input);
        if (this.valid) {
            return this.input;
        } else {
            return 0;
        }
    }

    setToZero() {
        this.sum = 0;
        this.showAnswer();
    }

    static validateInput(input) {
        return !(isNaN(input) || input === '');
    }
}
