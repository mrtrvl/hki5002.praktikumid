class Field {
    constructor(layer) {
        this.lineBreak = document.createElement('br');

        this.box = document.createElement('input');
        this.box.type = 'number';

        layer.appendChild(this.lineBreak);
        layer.appendChild(this.box);
    }

    value() {
        this.input = this.box.value;
        this.valid = this.validateInput(this.input);
        if (this.valid) {
            return parseFloat(this.input);
        } else {
            return 0;
        }
    }

    validateInput(input) {
        return !(isNaN(input) || input === '');
    }
}