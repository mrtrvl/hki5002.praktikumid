class Calculator {
    constructor(layerName) {
        this.fields = [];

        this.element = document.createElement('div');
        this.element.setAttribute('id', layerName);

        this.answerField = document.createElement('div');

        this.addFieldButton = document.createElement('input');
        this.addFieldButton.type = 'button';
        this.addFieldButton.value = 'Lisa väli';
        this.addFieldButton.addEventListener('click', this.addField.bind(this));

        this.calculateButton = document.createElement('input');
        this.calculateButton.type = 'button';
        this.calculateButton.value = 'Arvuta';
        this.calculateButton.addEventListener('click', this.calculate.bind(this));

        document.querySelector('#Calculator').appendChild(this.element);

        this.element.appendChild(this.addFieldButton);
        this.element.appendChild(this.calculateButton);
        this.element.appendChild(this.answerField);
    }

    calculate() {
        this.sum = 0;
        this.value = 0;

        this.fields.forEach(function (field) {
            this.value = field.value();
            this.sum += this.value;
        }, this);

        this.average = this.sum / this.fields.length;

        this.answerField.innerHTML = 'Aritmeetiline keskmine: ' + this.average;
    }

    addField() {
        this.fields.push(new Field(this.element));
    }
}